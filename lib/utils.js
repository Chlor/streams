const pick = (object, keys) => Object.keys(object).reduce((p, key) => {
  if (keys.includes(key)) {
    p[key] = object[key]
  }
  return p
}, {})

module.exports = {pick}
