require('./lib/global')

const streams = require('./lib'),
      {wrap, read, map, consume, reduce} = streams

const resolver = stream => stream.pipe(consume((p, c) => [...p, c], []))

const slow = map(elem => new Promise(resolve => setTimeout(() => resolve(elem), 250)))

const x4 = map(elem => elem * 8)
const x8 = map(elem => elem * 16)

setInterval(() => console.log('...'), 250)

wrap(read(Array.from({length: 1e2}).map(Number.call, Number)))
//.map(elem => {
//  console.log(elem)
//  return elem
//})
//.map(elem => {
//  console.log(elem)
//  return 'last' + elem.toString() + '\n'
//})
//.source(stream => {
//  return resolver(stream).then(res => console.log(res))
//})
  .tee(x8)

  .then(res => {
    let reducer = reduce((p, c) => p + c)
    res.map(one => one.pipe(slow).pipe(reducer))
    return reducer
  })
  .then(res => {
    res
      .pipe(map(elem => elem.toString()))
      .pipe(process.stdout)
  })
//  .catch(err => console.log(err))
//.pipe(process.stdout)

//console.log('sync', Array.from({length: 1e6}).map((_, i) => i  * 16).reduce((p, c) => p + c))
console.log('test?')

process.stdin.resume()
process.stdin.pipe(process.stdout)
