const base = require('./base'),
      wrap = require('./wrap')

module.exports = Object.assign({}, base, {wrap})
