const {Readable, Writable, Transform} = require('stream')

const defaults = ({objectMode = true, encoding = null}) => {
  return {objectMode, encoding}
}

const read = (data, options = {}) => {
  const s   = new Readable(defaults(options)),
        len = data.length
  let i = 0
  s._read = function () {
    if (i === len) {
      return this.push(null)
    }
    this.push(data[i++])
  }
  return s
}

const transform = (transform, flush = null, options = {}) => {
  const s = new Transform(defaults(options))
  s._transform = transform
  if (flush) {
    s._flush = flush
  }
  return s
}

const write = (fn, options = {}) => {
  const s = new Writable(defaults(options))
  s._write = fn
  return s
}

const consume = (fn, aggr, options) => {
  const deferred = Promise.defer(),
        s        = write(function (c, e, d) {
          if (typeof aggr === 'undefined') {
            aggr = c
            return d()
          }
          Promise.resolve(fn(aggr, c)).then(res => {
            aggr = res
            d()
          })
        }, options)

  s.on('finish', () => deferred.resolve(aggr))
  s.on('error', deferred.reject)
  s.then = deferred.promise.then.bind(deferred.promise)
  s.catch = deferred.promise.catch.bind(deferred.promise)
  return s
}

const map = (fn, options) => transform(function (c, e, d) {
  Promise.resolve(fn(c)).then(res => {
    this.push(res)
    d()
  })
}, null, options)

const filter = (fn, options) => transform(function (c, e, d) {
  Promise.resolve(fn(c)).then(res => {
    res && this.push(c)
    d()
  })
}, null, options)

const reduce = (fn, aggr, options) => transform(
  function (c, e, d) {
    if (typeof aggr === 'undefined') {
      aggr = c
      return d()
    }
    Promise.resolve(fn(aggr, c)).then(res => {
      aggr = res
      d()
    })
  },
  function (d) {
    this.push(aggr)
    d()
  }, options)

module.exports = {read, transform, write, consume, map, filter, reduce}
