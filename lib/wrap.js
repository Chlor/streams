const {pick} = require('./utils'),
      baseMethods = pick(require('./base'), [
        'map', 'reduce', 'filter', 'consume'
      ])

const bind = (stream, method, shouldWrap) =>
  (...args) =>
    stream.pipe.call(stream,
      shouldWrap
        ? wrap(method.call(stream, ...args))
        : method.call(stream, ...args)
    )

const tee = (stream, ...dests) => Promise.all(
  dests.map(d => Promise.resolve(stream.pipe(d)))
)

const handler = {
  get(target, key) {
    if (key === 'unwrap') {
      return () => target
    }
    if (key === 'source') {
      return (fn = null) => fn ? fn(wrap(target)) : wrap(target)
    }
    if (key === 'tee') {
      return (...args) => tee(target, ...args)
    }
    if (baseMethods.hasOwnProperty(key)) {
      return bind(target, baseMethods[key], true)
    }
    if (typeof target[key] === 'function') {
      return (...args) => target[key].call(target, ...args)
    }
    return target[key]
  }
}

function wrap(stream) {
  return new Proxy(stream, handler)
}

module.exports = wrap

console.log(Object.getOwnPropertyDescriptor({get test() {return 'test'}}, 'test'))
